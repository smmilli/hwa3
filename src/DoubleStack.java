import java.util.Arrays;
import java.util.List;

//Hilinemine -25%. Meetodid op ja tos peavad samuti ise kontrollima alatäitumist.
//        Kõigile erinditele lisada sisukad veateated. Silumistrükid võib eemaldada.

public class DoubleStack {
   private class Node {

      double data;
      Node next;

      public Node(double data) {
         this.data = data;
      }

      public Node(double data, Node next) {
         this.data = data;
         this.next = next;
      }
   }
   Node head;

   public static void main (String[] argum) {
      DoubleStack ds = new DoubleStack();

      System.out.println(ds);

      String s1 = "2 5 SWAP -";
      String s2 = "2 5 9 ROT - +";
      String s3 = "2 5 9 ROT + SWAP -";
      System.out.println(interpret(s1));
      System.out.println(interpret(s2));
      System.out.println(interpret(s3));

   }

   DoubleStack() {
      this.head = null;
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack ds = new DoubleStack();
      Node current = head;
      Node newHead = null;

      while(current != null){
         if (newHead == null){
            newHead = new Node(current.data, newHead);
            ds.head = newHead;
         }
         else {
            ds.head.next = new Node(current.data, ds.head.next);
         }
         current = current.next;
      }
      return ds;
   }

   public boolean stEmpty() {
//      return false; // TODO!!! Your code here!
      return head == null;
   }

   public void push (double a) {
      // TODO!!! Your code here!

      Node helperHead = head;
      head = new Node(a);
      head.data = a;
      head.next = helperHead;

   }

   public double pop() {
      if (stEmpty()){
        throw new RuntimeException("Cant pop a element, Stack is empty");
      }
      double data = head.data;
      head = head.next;
      return data;

   }

   public void op (String s) {
      if (stEmpty() || head.next == null){
         throw new RuntimeException("Not enough elements in stack to run aritmethic calculation.");
      }

      double a = pop();
      double b = pop();
//      System.out.println(a);
//      System.out.println(b);
      s = s.trim();
      if (s.equals("+")){
         push(b + a);
      } else if (s.equals("-")) {
         push(b-a);
      } else if (s.equals("*")){
         push(b * a);
      } else if (s.equals("/")){
         push(b/a);
      }
   }
  
   public double tos() {
      if (stEmpty()){
         throw new RuntimeException("Can't read data as there's no element to read it from");
      }
      return head.data;
   }

   @Override
   public boolean equals (Object o) {
      Node b = null;
      if (o instanceof DoubleStack){
         DoubleStack other = (DoubleStack) o;
         b = other.head;
      } else {
         return false;
      }
      Node a = this.head;
      while (a != null){
         if (a.data != b.data) {
            return false;
         }
         a = a.next;
         b = b.next;
      }
      return a == b;
   }
   @Override
   public String toString() {
      StringBuilder str = new StringBuilder();
      DoubleStack b = new DoubleStack();
      Node current = head;
      while (current != null){
         b.push(current.data);
         current = current.next;
      }
      Node current2 = b.head;
      while(current2 != null){
         str.append(current2.data).append(" ");
         current2 = current2.next;
      }
      return str.toString();


   }
   public static double interpret (String pol) {
      if (pol == null || pol.equals("")){
         throw new RuntimeException("No expression: " + pol);
      }
      String [] elements = pol.trim().split(" ");
      List<String> operators = Arrays.asList("+", "-", "/", "*");
      List<String> stackOperations = Arrays.asList("SWAP", "ROT");

      DoubleStack ds = new DoubleStack();
      boolean numeric = true;
      Double num = 0.;
      for (String x: elements) {
         if (x.equals("") || x.equals("\t")){
            continue;
         }
         try {
            num = Double.parseDouble(x);
            numeric = true;
         } catch (NumberFormatException e) {
            numeric = false;
         }

         if (stackOperations.contains(x)){
            if (ds.head == null || ds.head.next == null) throw new RuntimeException("Not enough elements to run " + x + " operation");
            double a = ds.pop();
            double b = ds.pop();
           if (x.equals(stackOperations.get(0))){
               ds.push(a);
               ds.push(b);
            } else  {
               if (ds.head == null) throw new RuntimeException("Not enough elements to run " + x + " operation");
               double c = ds.pop();
               ds.push(b);
               ds.push(a);
               ds.push(c);
            }
         }

         else if (operators.contains(x)){
            try {
               ds.op(x);
            } catch (RuntimeException e) {
               throw new RuntimeException("Not enough numbers in expression: " + pol);
            }
         }
         else if (numeric) {
            ds.push(num);
         }
         else {
            throw new RuntimeException("Unknown character in expression: " + pol);
         }
      }
      if (ds.head.next != null){
         throw new RuntimeException("Too many numbers in expression: " + pol);
      } else {
         return ds.head.data;
      }
   }
}

